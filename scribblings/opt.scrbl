#lang scribble/manual
@require[@for-label[opt/option
                    opt/either
                    racket/base]]	

@title{Opt}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Option}
@defmodule[option]

Utility functions useful for Typed Rackets @racket[Option] type.

@defproc[(opt-car [lst (Listof a)]) (Option a)]{
  @racket[(: opt-car (All (A) ((Listof A) -> (Option A))))]
  
  Total head function for a hetergeneous list.
  If the list is non-empty returns the first element else #f.						      
}						

@defproc[(opt-undefined? [x (Option a)]) Boolean]{
@racket[(: opt-defined? (All (a) (Option a) -> Boolean))]

Is the option value defined? Of little to no utility, but provided for completeness.
}

@defproc[(opt-exists? [x (Option a)] [pref (a -> Boolean)]) Boolean]{
@racket[(: opt-exists (All (a) (Option a) (a -> Boolean) -> Boolean))]

Does the option value exist and does the value match the provided predicate?
}

@defproc[(opt-get-orelse [x (Option a)][def-expr (Thunk a)]) a] {
@racket[(: opt-get-orelse (All (a) (Option a) (-> a) -> a))]

Returns the option's value if defined else the value returned by the provided thunk.
}

@defproc[(opt-get-orelse-value [x (Option a)][def-value a]) a]{
@racket[(: opt-get-orelse-value (All (a) (Option a) a -> a))]

Returns the options' value if defined else the provided default value.
}

@defproc[(opt-orelse [x (Option a)] [def-expr (-> (Option a))]) (Option a)] {
@racket[(: opt-orelse (All (a) (Option a) (-> (Option a)) -> (Option a)))]

Returns the provided  option itself if its value is defined otherwise the given alternative option.
}

@defproc[(opt-map [x (Option a)][fn (-> a b)]) (Option b)]{
(: opt-map (All (a b) (Option a) (a -> b) -> (Option b)))

Apply the given procedure to the option value if defined.
}

@defproc[(opt-map-orelse-value [x (Option a)] [fn (-> a b)][default b]) b]{
@racket[(: opt-map-orelse-value (All (a b) (Option a) (a -> b) b -> b ))]

Apply the given procedure to the option's value if defined and return it, otherwise return the provided  default value.
}

@defproc[(opt-map-orelse [x (Option a)] [fn (-> a b)] [default-expr (Thunk b)]) b]{
@racket[(: opt-map-orelse (All (a b) (Option a) (a -> b) (-> b) -> b))]

Apply the given procedure to the option's value if defined and return it, otherwise return the value returned from the provided thunk when evaluated.
}

@defproc[(opt-flatmap [x (Option a)][fmap (-> a (Option b))]) (Option b)]{
@racket[(: opt-flatmap (All (a b) (Option a) (a -> (Option b)) -> (Option b)))]

Use the provided function to translate from an option value of one type to an option value of another type.
}

@defproc[(opt-filter [x (Option a)][fn (-> a Boolean)]) (Option a)]{
@racket[(: opt-filter (All (a) (Option a) (a -> Boolean) -> (Option a)))]

If the option is defined and the predicate is satisfied return the option's value else @racket[#f].
}

@defproc[(opt-reject [x (Option a)] [fn (-> a Boolean)]) (Option a)]{
@racket[(: opt-reject (All (a) (Option a) (a -> Boolean) -> (Option a)))]

Drop's the options value (i.e. returns #f) if the value does not match the provided predicate.
}

@defproc[(opt-foreach [x (Option a)][proc (-> a Void)]) Void]{
@racket[(: opt-foreach (All (T) (Option T) (T -> Void)  -> Void))]

Apply a side-effecting procedure to the option's value if defined, otherwise does nothing.   Returns a void value.
}

@section{Either}

@defmodule[either]

@defstruct[Left ([val E])]{
@racket[(struct: (E) Left  ([val : E]) #:transparent)]

The left possible value of an Either type.  By convention is often the error or failure value or message.
}

@defstruct[Right ([val D])]{
@racket[(struct: (D) Right ([val : D]) #:transparent)]

The right possible value of an Either type.  By convention is often the correct or success value.
}

@defthing[#:kind "Type" Either (U (Left E) (Right D))]{
@racket[(define-type (Either E D) (U (Left E) (Right D)))]

A type whose value is of one type or another.  By convention often the Left type value is a failure value  value and the Right type value is a success value.
}

@defproc[(left [x (Either E D)]) E]{
@racket[(: left (All (E D) (Either E D) -> E))]

Project (return) the Either's value IF it is a Left value, otherwise and @racket[error] is thrown.
}

@defproc[(right [x (Either E D)]) D]{
@racket[(: right (All (E D) (Either E D) -> D))]

Project (return the Either's value IF it is a @racket[Right] value, otherwise an @racket[error] is thrown.
}

@defproc[(either [left-fn (-> E T)] [right-fn (-> D T)] [an-either (Either E D)]) T]{
@racket[(: either (All (E D T) ((E -> T) (D -> T) (Either E D) -> T)))]

Applies one of the provided procedures to the value of the provided @racket[Either] argument.  Which procedure is applied depends on the type of the
provided Either's value.
}
